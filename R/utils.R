Library <- function(pkg, ...){
  
  PACK <- .packages(all.available=TRUE)
  pkgc <- deparse(substitute(pkg))
  
  if(!pkgc %in% PACK){
    install.packages(pkgc, ...)
  }
  library(pkgc, character.only=TRUE)
}
