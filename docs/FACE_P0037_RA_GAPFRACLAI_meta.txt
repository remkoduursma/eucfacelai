Estimates of canopy gap fraction and leaf area index for the EucFACE based on diffuse transmittance.

This file is 'OPEN', more data will be added continuously. 

Data processing follows methods from Duursma et al. (2015, Global Change Biology, doi: 10.1111/gcb.13151).

Transmittance is estimated as PAR below / PAR above canopy, for 30min periods where
the diffuse fraction > 0.98 (as estimated from the CUP Eddy flux tower site).

Estimates of gap fraction and leaf area index (LAI) are daily averages by FACE Ring. 
The number of observations varies between days, depending on the number of 30min cloudy periods.

The code to generate these estimates can be accessed at https://www.bitbucket.org/remkoduursma/eucfacelai

Variables:
   Date - YYYY-MM-DD
   Ring - One of six EucFACE rings (R1, R2, ..., R6)
   treatment - Either 'elevated' or 'ambient'
   Gapfraction.mean - Daily mean gapfraction (0-1)
   Rain_mm_Tot.mean - Daily rainfall (mm)
   Gapfraction.sd - within-day standard deviation of gapfraction
   Gapfraction.length - Number of observations used for gapfraction estimate in a day (nr. 30min timesteps)
   LAI - Inferred leaf area index (m2 m-2) using algorithm of Campbell&Norman 2000, based on the
   attenuation of diffuse radiation in a homogenous canopy.
